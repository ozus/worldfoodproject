<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', ['as' => 'root', 'uses' => 'MealsController@getBuilderForm']);
Route::get('/food', 'MealsController@buildMeals');
//for ajax soulution
Route::get('ajaxFood/{time?}', ['as' => 'ajax.timeDiff', 'uses' => 'MealsController@ajaxReturnUnixTimeDiff']);



