@extends('layouts.master')

@section('title')
Build Request
@endsection

 @section('content')   
{{Html::style('datetimepicker/jquery.datetimepicker.css')}}   
        
        <div class="title">
        	Request Builder
        </div>
    
        <form action="{{url('/food')}}" method="get">
            {{ csrf_field() }}
            <div class="select-response">
            	<div class="row">
	            	<div class="col-md-8 col-lg-8 col-sm-10 col-xs-10 col-lg-push-2 col-md-push-2">
	            		<div class="form-group">
	            			<label>Select response type: </label>
		            		<select class="form-control" name="response" >
		            			<option value="html">Html</option>
		            			<option value="json">JSON</option>
		            			<option value="dd">Drop and Die</option>
		            		</select>
	            		</div>
	            	</div>
            	</div>
            </div>
            <div class="form-group">
                <label>Category: </label>
                <select class="form-control" name='category'>
                    <option value="all">All</option>
                    <option value="n">NULL</option>
                    @foreach($cats as $cat_id)
                        <option value="{{$cat_id}}">{{$cat_id}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Tag: </label>
                <select id="tagSelect" class="form-control" name="tags[]"  multiple>
                    @foreach($tags as $tag_id)
                        <option value="{{$tag_id}}">{{$tag_id}}</option>
                    @endforeach
                </select>
                <button type="button" class="btn btn-default" onclick="selectAll();">Select All</button>
                <button type="button" class="btn btn-default" onclick="clearAll();">Clear All</button>
            </div>
            <div class="form-group">
                <label>Language: </label>
                <select class="form-control" name="lang">
                    <option value="ro">ro</option>
                     @foreach($langs as $lang)
                        <option value="{{$lang->short}}">{{$lang->short}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Time Difference(<small>Only enterys created, modified or deleted after this date, will be returned</small>): </label> 
                <div id="diffResult"></div>
                <input id="timeDiff" class="form-control" type="text" name="diff" />
            </div>
            <div class="form-group">
                <label>Results per page: </label>
                <select class="form-control" name="per_page">
                    <option value="all">Show All</option>
                    @for($i = 1; $i < 6; $i++)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </div>
            <div class="form-group">
                <label>With: </label>
                <select class="form-control" name="multi[]" multiple="">
                    <option value="category">Category</option>
                    <option value="tags">Tags</option>
                    <option value="ingridiants">Ingridiants</option>
                </select>
            </div>
            <input class="btn btn-primary" type="submit" value="Send Query" />
        </form>

{{Html::script('datetimepicker/jquery.js')}}
{{Html::script('datetimepicker/build/jquery.datetimepicker.full.min.js')}}
<script>
jQuery('#timeDiff').datetimepicker({
	format:'Y-m-d H:i'
});

var time = document.getElementById('timeDiff');

time.onchange = function() {
	//ajax solution
	
	/* var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(this.readyState == 4 && this.status == 200) {
			document.getElementById('diffResult').innerHTML = this.responseText;
		}
	}
	xhttp.open("GET", "{{route('ajax.timeDiff')}}" + "/" + encodeURIComponent(this.value), true);	
	xhttp.send(); */
	
	//without ajax
	$date = new Date(this.value).getTime() / 1000;
	document.getElementById('diffResult').innerHTML = $date;
	
}

function selectAll() {
	var tags = document.getElementById('tagSelect');
	for(var i = 0; i < tags.options.length; i++) {
		tags.options[i].selected = true;
	}
}

function clearAll() {
	document.getElementById('tagSelect').value = false;
}




</script>        

@endsection