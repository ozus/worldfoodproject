@extends('layouts.master')

@section('title')
List
@endsection

@section('content')

	<div class="title">
		Result
	</div>
	
	@for($i = 0; $i < count($meals); $i++)
		<div class="response-content">
			<ul>
				<li>{{$meals[$i] -> id}}</li>
				<li>{{$meals[$i] -> title}}</li>
				<li>{{$meals[$i] -> desc}}</li>
				<li>{{$meals[$i] -> status}}</li>
				<li>{{$meals[$i] -> slug}}</li>
			</ul>
			@if($cats != "none" && $cats[0] != null)
			<div class="row">
				<div class="col-md-10 col-lg-10 col-md-push-1 col-lg-push-1">
					Category:
					<ul>
						<li>{{$cats[$i] -> id}}</li>
						<li>{{$cats[$i] -> title}}</li>
						<li>{{$cats[$i] -> desc}}</li>
					</ul>
				</div>
			</div>
			@endif
			@if($tags != "none")
			<div class="row">
				<div class="col-md-10 col-lg-10 col-md-push-1 col-lg-push-1">
					Tags:
					@foreach($tags[$i] as $tag)
						<ul>
							<li>{{$tag -> id}}</li>
							<li>{{$tag -> title}}</li>
							<li>{{$tag -> desc}}</li>
						</ul>
					@endforeach
				</div>
			</div>
			@endif
			@if($ingridiants != "none")
			<div class="row">
				<div class="col-md-10 col-lg-10 col-md-push-1 col-lg-push-1">
					Ingridiants:
					@foreach($ingridiants[$i] as $ingridiant)
						<ul>
							<li>{{$ingridiant -> id}}</li>
							<li>{{$ingridiant -> title}}</li>
							<li>{{$ingridiant -> desc}}</li>
						</ul>
					@endforeach
				</div>
			</div>
			@endif
		</div>	
	@endfor

	@if($paged)
	 {{ $orig->appends(request()->input())->links() }}
	@endif

	<a class="back-btn" href="{{url('/')}}" >Back</a>
@endsection