<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cat_id') -> unsigned() -> nullable();
            $table->foreign('cat_id')->references('id')->on('categories')->onDelete('cascade');
            $table->integer('lang_id');
            $table->string('title', 45)->nullable();
            $table->string('desc', 100)->nullable();
            $table->string('status', 45);
            $table->integer('created_at');
            $table->integer('updated_at') -> nullable();
            $table->integer('deleted_at') -> nullable();
            $table->string('slug', 400)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meals');
    }
}
