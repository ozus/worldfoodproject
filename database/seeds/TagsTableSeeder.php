<?php

use Illuminate\Database\Seeder;
use App\Tag;

class TagsTableSeeder extends Seeder
{	
	protected $faker;

	public function __construct(Faker\Generator $faker) {
		$this -> faker = $faker;
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1,100) as $index) {
       
        	$tag = new Tag();

        	$tag->id = $index;
        	$tag->lang_id = 101 - $index;
        	//Ensures that every meal_id is selected at least once
        	if($index < 21) {
        		$tag->meal_id = $index;
        	} else {
        		$tag->meal_id = $this->faker->numberBetween($min = 1, $max = 20);
        	}
        	$tag->slug = $this->faker->slug;

        	$tag->save();

        }
    }
}
