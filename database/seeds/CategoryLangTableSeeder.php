<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Fake;

class CategoryLangTableSeeder extends Seeder
{

	protected $faker;
	protected $fr_faker;
	protected $de_faker;
	protected $hr_faker;

	public function __construct(Faker\Generator $faker) {

		$this->faker = $faker;
		$this->fr_faker = Fake::create('fr_FR');
		$this->de_faker = Fake::create('de_DE');
		$this->hr_faker = Fake::create('hr_HR');
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1,10) as $index) {
        	DB::table('category_lang')->insert([
        		'id' => $index,
        		'lang_id' => 11 - $index,	
        		'en_title' => $this->faker->unique()->firstName,
        		'en_desc' => $this->faker->realText(100),
        		'fr_title' => $this->fr_faker->unique()->firstName,
        		'fr_desc' => $this->fr_faker->realText(100),
        		'de_title' => $this->de_faker->unique()->firstName,
        		'de_desc' =>  $this->de_faker->realText(100),
        		'hr_title' => $this->hr_faker->unique()->firstName,
        		'hr_desc' => $this->hr_faker->text
        	]);
        }
    }
}
