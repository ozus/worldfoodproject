<?php

use Illuminate\Database\Seeder;
use App\Ingridiant;

class IngridiantsTableSeeder extends Seeder
{
	protected $faker;

	public function __construct(Faker\Generator $faker) {
		$this -> faker = $faker;
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1,80) as $index) {
     
        	$ingridiant = new Ingridiant();

        	$ingridiant->id = $index;
        	$ingridiant->lang_id = 81 - $index;
        	//Ensures that every meal_id is selected at least once
        	if($index < 21) {
        		$ingridiant->meal_id = $index;
        	} else {
        		$ingridiant->meal_id = $this->faker->numberBetween($min = 1, $max = 20);
        	}
        	$ingridiant->slug = $this->faker->slug;

        	$ingridiant->save();
        }
    }
}
