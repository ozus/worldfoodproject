<?php

use Illuminate\Database\Seeder;
use App\Category as Category;


class CategoriesTableSeeder extends Seeder
{
	protected $category;
	protected $faker;

	public function __construct(Faker\Generator $faker) {
		$this -> faker = $faker;
        $this -> category = new Category();
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach(range(1,10) as $index) {

            $this->category->insert([
                'slug' => $this->faker->slug,
                'id' => $index,
            	'lang_id' => 11 - $index	
            ]);

        }


    }
}
