<?php

use Illuminate\Database\Seeder;


class LanguagesTableSeeder extends Seeder
{	

	protected $faker;
	protected $langs = array();
	protected $shorts = array();

	public function __construct(Faker\Generator $faker) {
		$this->faker = $faker;
        $this->langs = array('English', 'Franch', 'German', 'Croatian');
        $this->shorts = array('en', 'fr', 'de', 'hr');
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1; $i<5; $i++) {
        	DB::table('languages')->insert([
        		'id' => $i,
        		'short' => $this->shorts[$i-1],
        		'language' => $this->langs[$i-1]
        	]);
        }
    }
}
