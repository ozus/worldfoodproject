<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;
use App\Tag;
use App\Category;
use DB;

class MealsController extends Controller
{
	protected $request;
	protected $response;
	protected $meal;
	protected $tag;
	protected $category;
	protected $with = array();
	protected $paged = false;
	
	/**
	 * Injects needed objects and properties
	 *
	 * @param Object $request, Object $meal, Object $tag, Object $ category
	 */
	public function __construct(Request $request, Meal $meal, Tag $tag, Category $category) {
		$this -> request = $request;
		$this -> meal = $meal;
		$this -> tag = $tag;
		$this -> category = $category;
		$this -> with = $request->input('multi');
		$this -> response = $request->input('response');
	}
	
	/**
	 * Return front form for building request
	 *
	 * @return Response
	 */
	public function getBuilderForm() {
		$categoriesReturn = array();
		$tagsReturn = array();
		
		$categories = $this->category->all();
		foreach($categories as $category) {
			array_push($categoriesReturn, $category->id);
		}

		$tags = $this->tag->all();
		foreach($tags as $tag) {
			array_push($tagsReturn, $tag->id);
		}

		$languages = DB::table('languages') -> select('short') -> get();

		return view('welcome', ['cats' => $categoriesReturn, 'tags' => $tagsReturn, 'langs' => $languages]);

	}
	/**
	 * Build query from input request, sends colection for response creating
	 *
	 * @return view
	 */
    public function buildMeals() {

    	$query = $this->meal->filterByTag($this->request->input('tags')) -> filterByCategory($this->request->input('category')) -> filterByTimeDiff(strtotime($this->request->input('diff')));

    	if(!$this->request->has('diff')) {
    		$query -> where('status', '=', 'created');
    	}


    	if($this->request->input('per_page') == 'all') {
    	 	$meals = $query-> get();
    	} else {
    		$meals = $query-> simplePaginate($this->request->input('per_page'));
    		$this->paged = true;
    	}

    	foreach($meals as $meal) {
    		$this->withAndTranslation($meal, $this->request->input('lang'));
    	}

    	return $this->createAndSendResponse($meals);

    }

	//for ajax solution
    public function ajaxReturnUnixTimeDiff($time=NULL) {
    	return strtotime(urldecode($time));
    }


    /**
     * Adds translation strings and relations
     *
     * @param Object $meal, String $lang
     * @return Modified Object
     */
    protected function withAndTranslation(& $meal, $lang) {
    	if(!DB::table('languages')->where('short', '=', $lang)->exists()) {
    		$lang="en";
    	}

    	$this -> addTranslationStrings($meal, $lang, 'meals_lang');

    	if($this->request->has('multi')) {
    		if(in_array('category', $this->with)) {
	    		$cat = $meal->categories;
				if($cat) {
	    			$this -> addTranslationStrings($cat, $lang, 'category_lang');
    			}
	    	}
	    	if(in_array('tags', $this->with)) {
	    		$tags = $meal->tags; 

	    		foreach($tags as $tag) {

		    		$this -> addTranslationStrings($tag, $lang, 'tags_lang');

	    		}

	    	}
	    	if(in_array('ingridiants', $this->with)) {
	    		$ingridiants = $meal->ingridians;

	    		foreach($ingridiants as $ingridiant) {

		    		$this -> addTranslationStrings($ingridiant, $lang, 'ingridians_lang');

	    		}
	    	}
	    }
    }

    /**
     * Adds translation strings to object
     *
     * @param Object $subject, String $lng, String $table
     * @return Modified Object
     */
    protected function addTranslationStrings(& $subject, $lng, $table) {

    	$translationStrings = DB::table($table) -> select($lng . '_title', $lng . '_desc') -> where('lang_id', '=', $subject->lang_id) -> first();
	    $translationArray = (array) $translationStrings;
	    $finalTranslationArray = array_values($translationArray);

	    $subject->title = $finalTranslationArray[0];
	    $subject->desc = $finalTranslationArray[1];

    }

    /**
     * Creats response (Html, Json or dd)
     *
     * @param Collection $meals
     * @return View or Response
     */
    protected function createAndSendResponse($meals) {

    	switch($this->response) {
    		
    		case 'html' :    	
		    	for($i = 0; $i < $meals -> count(); $i++ ) {
		    			if($this->request->has('multi')) {
				    		if(in_array('category', $this->with)) {
				    			$responseCategories[$i] = $meals[$i]->categories;
				    		} else {
				    			$responseCategories = "none";
				    		}	
				    		if(in_array('ingridiants', $this->with)) {
				    			$responseIngridiants[$i] = $meals[$i]->ingridians;
				    		} else {
				    			$responseIngridiants = "none";
				    		}
				    		if(in_array('tags', $this->with)) {
				    			$responseTags[$i] = $meals[$i]->tags;
				    		} else {
				    			$responseTags = "none";
				    		}
			    		} else {
			    			$responseCategories = "none"; 
			    			$responseTags = "none"; 
			    			$responseIngridiants = "none";
			    		}
		    		
		    		$responseMeals[$i] = $meals[$i];
		    	}
				if(!$meals->isEmpty()) {
		    		return view('list_meals')
		    		->with('meals', $responseMeals) -> with('cats', $responseCategories) -> with('tags', $responseTags) 
		    		-> with('ingridiants', $responseIngridiants)->with('orig', $meals) -> with('paged', $this->paged);
				} else {
					return view('noResults');
				}
			
			break;
			
    		case 'json':
    			
    			return response() -> json($meals);
    			
    		break;
    		
    		case 'dd':
    			dd($meals);
    		break;	

    	}
	}

}
